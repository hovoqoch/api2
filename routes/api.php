<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'StartController@login');

Route::group(['middleware' => ['auth:api']], function() {

    Route::post('/items/show', 'ItemsController@show');
    Route::post('/items/store', 'ItemsController@store');
    Route::post('/items/update', 'ItemsController@update');
    Route::post('/items/delete', 'ItemsController@delete');

    Route::post('/logout', 'StartController@logout');
});

Route::middleware('auth:api')->post('/user', function (Request $request) {
    return $request->user();
});


