<?php

namespace App\Http\Controllers;

use Validator;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use Auth;
use DB;


class ItemsController extends Controller
{
    public $successStatus = 200;
    public $dangerStatus = 422;

    public function show(Request $request)
    {
        if(Auth::guard('api')->check()){

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return response()->json(
                    $validator->errors()->toArray(), $this->dangerStatus
                );
            }

            $id = (int)$request->id;
            $item = Item::find($id);
            if ($item) {
                return response()->json(
                    [
                        'status' => 1,
                        'message' => 'Item finded',
                        'data' => $item,

                    ], $this->successStatus);
            }
            else {
                return response()->json(array(
                    'status' => 0,
                    'message' => 'Item not finded',
                    'data' => array()
                ), $this->dangerStatus);
            }
        } else {
            return \Response::json(['type'=>'error',
                'message' => 'Unauthenticated user',
            ], $this->dangerStatus);
        }
    }

    public function store(Request $request)
    {
        if(Auth::guard('api')->check()){
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:items|max:255',
                'key' => 'required|max:25',
            ]);
            if ($validator->fails()) {
                return response()->json(
                    $validator->errors()->toArray(), $this->dangerStatus
                );
            }
            else {
                $item = new Item;
                $item->name = $request->name;
                $item->key = $request->key;
                $item->save();
                return response()->json([
                    'status' => 1,
                    'data' => $item,
                    'message' => 'The item was successfuly added'
                ], $this->successStatus);
            }
        } else {
            return \Response::json(['type'=>'error',
                'message' => 'Unauthenticated user',
            ], $this->dangerStatus);
        }
    }

    public function update(Request $request)
    {
        if(Auth::guard('api')->check()){

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric|exists:items,id',
                'name' => 'required|unique:items,name,'.(int)$request->id.'|max:255',
                'key' => 'required|max:25',
            ]);
            if ($validator->fails()) {
                return response()->json(
                    $validator->errors()->toArray(), $this->dangerStatus
                );
            }
            $id = $request->id;
            $item = Item::find($id);
            if ($item) {
                $item->name = $request->name;
                $item->key = $request->key;
                $item->save();
                return response()->json([
                    'status' => 1,
                    'data' => $item,
                    'message' => 'The item was successfuly updated'
                ], $this->successStatus);
            }
            else {
                return response()->json(array(
                    'status' => 0,
                    'message' => 'Item not finded',
                    'data' => array()
                ), $this->dangerStatus);
            }
        } else {
            return \Response::json(['type'=>'error',
                'message' => 'Unauthenticated user',
            ], $this->dangerStatus);
        }
    }

    public function delete(Request $request)
    {
      if(Auth::guard('api')->check()){
          $validator = Validator::make($request->all(), [
              'id' => 'required|numeric|exists:items,id'
          ],['id.exists'=>'Item not found']);

          if ($validator->fails()) {
              return response()->json(
                  $validator->errors()->toArray(), $this->dangerStatus
              );
          }
          $id = $request->id;
          $item = Item::find((int)$id);
          if ($item) {
              $item->delete();
              return response()->json([
                  'status' => 1,
                  'message' => 'The item was successfuly deleted'
              ], $this->successStatus);
          }
          else {
              return response()->json([
                  'status' => 0,
                  'message' => 'The item wasnt found',
                  'data' => array()
              ], $this->dangerStatus);
          }
      } else {
          return \Response::json(['type'=>'error',
              'message' => 'Unauthenticated user',
          ], $this->dangerStatus);
      }

    }


}
