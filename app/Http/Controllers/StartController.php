<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Item;
use App\User;
use Auth;
use DB;

class StartController extends Controller
{

    public $successStatus = 200;
    public $dangerStatus = 422;

    public function login()
    {
        $rules = [
            'password' => 'required',
            'email' => 'required|email',
        ];
        $validator = Validator::make(['email'=>request('email'),'password' => request('password')], $rules);

        if ($validator->fails()) {
            return \Response::json($validator->errors()->toArray(), $this->dangerStatus);
        }

        $user = User::where('email',request('email'))->first();

        if($user && isset($user->id)){

            $hasher = app('hash');
            if ($hasher->check(request('password'), $user->password)) {

                DB::table('oauth_access_tokens')->where('user_id',$user->id)->delete();

                Auth::loginUsingId($user->id);

                $user = Auth::user();
                $success['token'] =  $user->createToken('MyApp')->accessToken;

                return response()->json([
                    'success' => $success,
                    'data'=> [
                        'userID' => $user->id,
                        'name' => $user->name
                    ]
                ], $this->successStatus);

            } else {
                return \Response::json(['type'=>'error',
                    'message' => 'Email or Password is invalid',
                ], $this->dangerStatus);
            }

        } else {

            return \Response::json(['type'=>'error',
                'message' => 'User with current email address does not exists',
            ], $this->dangerStatus);
        }
    }

    public function logout()
    {
        if (Auth::guard('api')->check()) {

            $user_id = Auth::guard('api')->user()->id;

            try {

                Auth::guard('api')->user()->AauthAcessToken()->delete();

                return response()->json(['type' => 'success',
                    'message' => 'User is loged out',
                    'data' => [
                        'userID' => $user_id,
                    ]
                ], $this->successStatus);

            } catch (\Exception $e){

                return \Response::json(['type'=>'error',
                    'message' => 'Logout Process Failed',
                ], $this->dangerStatus);
            }
        } else {
            return \Response::json(['type'=>'error',
                'message' => 'Unauthenticated user',
            ], $this->dangerStatus);
        }
    }
}
